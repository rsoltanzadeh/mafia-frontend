<?php
session_start();
?>

<!DOCTYPE html>
<html id="app" lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="application/javascript" src="https://unpkg.com/vue@next"></script>
    <script type="application/javascript" src="javascript/index.js"></script>
    <link rel="stylesheet" type="text/css" href="css/index.css" />
    <link href="https://fonts.googleapis.com/css2?family=Syne+Mono&display=swap" rel="stylesheet">
    <title v-cloak> {{ title }} </title>
  </head>
  <body>
    <!-- NO-JS PAGE BEGIN -->
    <div id="no-js" v-if="false">
      <header>
	<h1 class="!caps lupara">The</h1>
	<h1 class="!caps bianca">Bluekin</h1>
	<p>- a game of organized crime and deception</p>
      </header>
      <main>
	<h1>JavaScript must be enabled.</h1>
      </main>
    </div>
    <!-- NO-JS PAGE END -->
    <!-- LOGIN PAGE BEGIN -->
    <div id="login-page" v-if="pages.atLogin" @keyup.enter="login" v-cloak>
      <header>
	<h1 class="!caps lupara">The</h1>
	<h1 class="!caps bianca">Bluekin</h1>
	<p> {{ message }} </p>
      </header>
      <main>
	<div id="login-form">
	  <p class="input-label">Username:</p>
  	  <input v-model="formInputs.username" type="text" name="username" class="input" v-focus autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" />
	  <br>
	  <p class="input-label">Password:</p>
	  <input v-model="formInputs.password" type="password" name="password" class="input" />
	  <br>
	  <button @click="login" id="participate" type="button">Sign in</button>
	  <div class="fixed">
	    <p class="input-label">Do not have an account?</p>
            <button @click="goto('register')">Register</button>
	    <a href="#" style="margin-top:50px"> Account recovery </a>
	  </div>
        </div>
      </main>
      <footer>
	<p> ` </p>
      </footer>
    </div>
    <!-- LOGIN PAGE END -->
    <!-- REGISTER PAGE BEGIN -->
    <div id="register-page" v-else-if="pages.atRegister" @keyup.enter="register" v-cloak>
      <div class="fixed">
	<p class="input-label">Already have an account?</p>
        <button @click="goto('login')">Sign in</button>
	<a href="#" style="margin-top:50px"> Account recovery </a>
      </div>
      <header>
	<h1 class="lupara">The</h1>
	<h1 class="bianca">Bluekin</h1>
	<p> {{ message }} </p>	
      </header>
      
      <main>
	<div id="register-form">
	  <p class="input-label">Enter e-mail address:</p>
	  <input v-model="formInputs.email" type="text" name="email" class="input" v-focus autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
	  <br>
  	  <p class="input-label">Choose username:</p>
  	  <input v-model="formInputs.username" type="text" name="username" class="input" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">	
	  <br>
          <p class="input-label">Create password:</p>
	  <input v-model="formInputs.password" type="password" name="password" class="input">
	  <br>
	  <button @click="register" id="register" class="submit">Register</button>
        </div>
      </main>
      <footer>
	<p> ` </p>
      </footer>

    </div>
    <!-- REGISTER PAGE END -->
    <!-- HOME PAGE BEGIN -->
    <div id="home-page" v-else-if="pages.atHome" v-cloak>      
      <header>
	<div id="options-wrapper">
	  <div onclick="" id="options"><p>Options</p></div>
	</div>
	<p id="message"> Press "Play" to enter the lobby. </p>
	<div id="user">
	  <p id="name">John Doe</p>
	  <div id="coins"> 
	    <p>Coins</p>
	  </div>
	  <div id="emblems">
	    <p>Emblems</p>
	  </div>
	</div>
	<div id="picture"><p>Avatar</p></div>
      </header>

      <aside>
	<div class="nav" onclick="" id="home"><p>Main</p></div>
	<div class="nav" onclick="" id="profile"><p>Profile</p></div>
	<div class="nav" onclick="" id="market"><p>Market</p></div>
	<div class="nav" onclick="" id="medals"><p>Medals</p></div>
      </aside>

      <main id="mainView">  
	<div id="play-wrapper">
	  <a @click="goto('lobby')" id="play"><p>Play</p></a>
	</div>
	<!-- <section id="ad">
	     </section>
	     <section id="discount">
	     </section>  

	     <section id="new">
	     </section>

	     <section id="live">
	     </section> -->  	 
      </main>
      <!--
	   <div bid="profileView" class="addition">
	   </div>

	   <div id="marketView" class="addition">
	   </div>

	   <div id="medalsView" class="addition">
	   </div>
      -->
      <footer>
	<div id="chat-container"></div>
	<div class="footer-item" id="friends"><p>Friends</p></div>
	<div class="footer-item" id="notifications"><p>Notifications</p></div>
      </footer>
      <div id="friends-window">
	<div id="friends-window-wrapper">
	  <div id="friends-list-wrapper">
	  </div>
	  <div id="friends-buttons-wrapper">
	    <div><p>Add friend: </p><input type="text" id="friend-input"/><button id="friend-button" type="button">Submit</button></div>
	    <div><p>Add foe: </p><input type="text" id="foe-input"/><button type="button" id="foe-button">Submit</button></div>
	    <div><p>Remove friend: </p><input type="text" id="remove-friend-input"/><button id="remove-friend-button" type="button">Submit</button></div>
	    <div><p>Remove foe: </p><input type="text" id="remove-foe-input"/><button id="remove-foe-button" type="button">Submit</button>
	    </div>
	  </div>
	</div>
      </div>
      <div id="notifications-window">
      </div>
    </div>
    <!-- HOME PAGE END -->
    <!-- LOBBY PAGE BEGIN -->
    <div id="lobby-page" v-else-if="pages.atLobby" v-cloak>
      <p class="title">Lobby</p>
      <p class="left-text">This is the lobby where players will gather in rooms whose host will choose the setup of the game. The players will chat with the others in the room while waiting for the game to begin.
	
	<br><br>
	
	At the moment this page is a public chat room as a demonstration of the client-server setup for the game.

	<br><br>

      </p>
      <p class="right-text"></p>
      <ul id="room-list">
	<li v-for="historyItem in roomHistory">
	  <p v-if="historyItem.hasOwnProperty('author')"><span class="author">{{ "[" + historyItem.timestamp + "] " + historyItem.author}}: </span>{{ historyItem.message }} </p>
	  <p v-else><span class="notification">{{ "[" + historyItem.timestamp + "] " + historyItem.message }}</span></p>
	</li>
	<li id="anchor"></li>
      </ul>
      <input v-if="publicChatConnected" @keyup.enter="sendPublicMessage" v-model="roomMessage" v-focus type="text" id="message-input"/>
      <button v-if="!publicChatConnected" @click="connectLobby" class="lobby-button" id="connect-button">
        <p>Connect to chat</p>
      </button>
      
      <button v-if="publicChatConnected" @click="sendPublicMessage" class="lobby-button" id="send-button">
	<p>Send message</p>
      </button>
      <button @click="disconnectLobby(); goto('home');" class="lobby-button" id="goto-home">
	<p>Leave Lobby</p>
      </button>
    </div>
    <!-- LOBBY PAGE END -->
    <!-- ROOM PAGE BEGIN -->
    <div id="room-page" v-else-if="pages.atRoom" @keyup.enter="roomMessageSend">	
      <div id="chat-wrapper">
	<div id="chat-window">
	  <p v-for="historyItem in roomHistory"> {{ historyItem }} </p>
	</div>
	<div id="leave-room-button">
	  <p>Leave room</p>
	</div>
      </div>
      
      <ul id="chosen-roles-list">
      </ul>

      <ul id="role-list">
      </ul>

      <div id="start-game-button">
	<p>Start game</p>
      </div>
    </div>
    <!-- ROOM PAGE END -->
    <!-- GAME PAGE BEGIN -->
    <div id="game-page" v-else-if="pages.atGame" v-cloak>
    </div>
    <!-- GAME PAGE END -->
  </body>
</html>
