let websocketConnection;
let lobbyConnected = false;
let scrollFlag = true;
const domainName = "thebluekin.com";

const NORMAL_MESSAGE = {
    GET_ROOMS : "get-rooms",
    CREATE_ROOM : "create-room",
    JOIN_ROOM : "join-room",
    LEAVE_ROOM : "leave-room",
    ADD_ROLE : "add-role",
    REMOVE_ROLE : "remove-role",
    START_GAME : "start-game",
    MESSAGE : "message",
    AUTHENTICATOR : "authenticator"
};

const NORMAL_RESPONSE = {
    JOINED_ROOM : "joined-room",
    LEFT_ROOM : "left-room",
    USER_JOINED_ROOM : "user-joined-room",
    USER_LEFT_ROOM : "user-left-room",
    ROOMS : "rooms",
    CHOSEN_ROLES : "chosen-roles",
    ROLES : "roles",
    AUTHENTICATED : "authenticated",
    MESSAGE : "message",
    HOST : "host",
    GAME_TOKEN : "game-token"
}

window.onload = function() {
    const app = Vue.createApp({
	data() {
	    return {
		publicChatConnected: false,
		username: "",
		pages: {
		    atLogin: true,
		    atRegister: false,
		    atHome: false,
		    atLobby: false,
		    atRoom: false,
		    atGame: false,
		},
		title: "The Bluekin",
		message: "- a game of organized crime and deception",
		formInputs: {
		    username: "",
		    password: "",
		    email: ""
		},
		lobbyRooms: [],
		roomMessage: "",
		roomHistory: []
	    }
	},
	methods: {
	    goto(page) {
		let obj = this;

		for(const p in obj.pages) {
		    obj.pages[p] = false;
		}
		
		switch(page) {
		case "login":
		    obj.pages.atLogin = true;
		    break;
		case "register":
		    obj.pages.atRegister = true;
		    break;
		case "home":
		    obj.pages.atHome = true;
		    break;
		case "lobby":
		    obj.pages.atLobby = true;
		    break;
		case "room":
		    obj.pages.atRoom = true;
		    break;
		case "game":
		    obj.pages.atGame = true;
		    break;
		}
	    },
	    
	    login() {
		let obj = this;
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
		    if(this.readyState == 4 && this.status ==  200) {
			if(this.responseText == "success") {
			    obj.goto("home");
			} else {
			    obj.message = this.responseText;
			}
		    }
		}
		xhr.open("POST", "../php/login.php");
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send("username=" + this.formInputs.username + "&password=" + this.formInputs.password);
	    },
	    
	    register() {
		let obj = this;
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
		    if(this.readyState == 4 && this.status ==  200) {
			if(this.responseText == "success") {
			    obj.goto("login");
			    obj.message = "Registration successful!";
			} else {
			    obj.message = this.responseText;
			}
		    }
		}
		xhr.open("POST", "../php/register.php");
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send("username=" + this.formInputs.username + "&password=" + this.formInputs.password + "&email=" + this.formInputs.email);
	    	
	    },

	    connectLobby() {
		let obj = this;
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
		    if(this.readyState == 4) {
			if(this.status == 200) {
			    connect(JSON.parse(this.responseText));
			} else {
			    alert("There was a problem fetching the authenticator: (HTTP status: " + this.status + ")");
			}
		    }
		}
		xhr.open("GET", "https://" + domainName + "/php/get_auth.php");
		xhr.send();

		function connect(authJSON) {
		    obj.username = authJSON.username;
		    websocketConnection = new WebSocket("wss://" + domainName + ":8443/lobby-server/lobby");
		    websocketConnection.onopen = function(e) {
			websocketConnection.send(NORMAL_MESSAGE.AUTHENTICATOR + ":" + obj.username);
			obj.lobbyConnected = true;
			obj.publicChatConnected = true;
		    };
		    websocketConnection.onmessage = function(e) {
			console.log("onmessage: " + e.data);
			let d = new Date();
			let timestamp = d.getHours().toString().padStart(2, '0') + ":" + d.getMinutes().toString().padStart(2, '0');
			if(e.data.startsWith(NORMAL_RESPONSE.MESSAGE)) {
			    let payload = e.data.substring(NORMAL_RESPONSE.MESSAGE.length + 1);
			    let author = payload.substring(0, payload.indexOf(':'));
			    let message = payload.substring(payload.indexOf(':') + 1);
			    obj.roomHistory.push(
				{
				    "timestamp": timestamp,
				    "author": author,
				    "message": message
				}
			    );
			    if(scrollFlag && document.getElementById("room-list").scrollHeight > document.getElementById("room-list").clientHeight) {
				document.getElementById("room-list").scrollTop += 1000; //exaggerate to make sure bottom is reached
				scrollFlag = false; // scroll once to pin to bottom this only once
				}
			} else if(e.data.startsWith(NORMAL_RESPONSE.USER_JOINED_ROOM)) {
			    let user = e.data.substring(NORMAL_RESPONSE.USER_JOINED_ROOM.length + 1);
			    let message = user + " has joined the lobby!";
			    obj.roomHistory.push(
				{
				    "timestamp": timestamp,
				    "message": message
				}
			    );
			} else if(e.data.startsWith(NORMAL_RESPONSE.USER_LEFT_ROOM)) {
			    let user = e.data.substring(NORMAL_RESPONSE.USER_LEFT_ROOM.length + 1);
			    let message = user + " has left the lobby.";
			    obj.roomHistory.push(
				{
				    "timestamp": timestamp,
				    "message": message
				}
			    );
			} else if(e.data.startsWith(NORMAL_RESPONSE.AUTHENTICATED)) {
			    let arrayOfUsers = e.data.substring(NORMAL_RESPONSE.AUTHENTICATED.length + 1);
			    arrayOfUsers = arrayOfUsers.substring(1, arrayOfUsers.length - 1).split(",");
			    let message = "Users currently online: ";
			    for(let i = 0; i < arrayOfUsers.length; i++) {
				message += arrayOfUsers[i];
				if(i < (arrayOfUsers.length - 1)) {
				    message += ", ";
				} else {
				    message += ".";
				}
			    }
			    obj.roomHistory.push(
				{
				    "timestamp": timestamp,
				    "message": message
				}
			    );			    
			}
		    };

		    websocketConnection.onclose = function(e) {		
			console.log("OnClose: " + e.reason);
		    };

		    websocketConnection.onerror = function(e) {
			console.log("OnError: " + e.reason);
		    };
		}
	    },

	    sendPublicMessage() {
		let temp = this.roomMessage; // will otherwise be able to modify message after clicking on "send"
		this.roomMessage = "";
		websocketConnection.send(NORMAL_MESSAGE.MESSAGE + ":" + temp);
	    },
	    
	    createRoom() {
		
	    },

	    roomMessageSend() {
		if(this.roomMessage == "" || !lobbyConnected)
		    return;
		websocketConnection.send(NORMAL_MESSAGE.MESSAGE + this.roomMessage);
		this.roomMessage = "";
	    },

	    disconnectLobby() {
		websocketConnection.close();
		this.lobbyConnected = false;
		this.publicChatConnected = false;
		this.roomHistory = [];
	    }
	}
    });

    app.directive('focus', {
	mounted(el) {
	    el.focus()
	}
    });
    const vm = app.mount("#app");
}
