window.onload = init;

var domainName;
var websocket;
var myUserName;
var aliasWindow;
var aliasInput;
var aliasSendButton;
var chatWindow;
var chatInput;
var chatSendButton;
var targetWindow;

var onTrial;
var target;
var secondTarget;
var vote;

var NORMAL_MESSAGE = {
    GAME_TOKEN : "game-token",
    AUTHENTICATOR : "authenticator",
    ALIAS : "alias",
    MESSAGE : "message",
    TRIAL_VOTE_ADD : "trial-vote-add",
    TRIAL_VOTE_REMOVE : "trial-vote-remove",
    VOTE_INNOCENT : "vote-innocent",
    VOTE_GUILTY : "vote-guilty",
    VOTE_REMOVE : "vote-remove",
    TARGET : "target",
    SECOND_TARGET : "second-target",
    REMOVE_TARGET : "remove-target",
    REMOVE_SECOND_TARGET : "remove-second-target"
    
};

var NORMAL_RESPONSE = {
    AUTHENTICATED : "authenticated",
    TOKEN_RECEIVED : "token-received",
    ALIAS : "alias",
    FIRST_DAY : "first-day",
    CASUALTIES : "casualties",
    CURRENT_STATE : "current-state",
    CHAT_ENABLED : "chat-enabled",
    TRIAL_OPEN : "trial-enabled",
    TRIAL_VOTE_ADDED : "trial-vote-added",
    TRIAL_VOTE_REMOVED : "trial-vote-removed",
    TRIAL_INTRO : "trial-intro",
    TRIAL_DEFENSE : "trial-defense",
    TRIAL_VOTING_ENABLED : "trial-voting-enabled",
    TRIAL_VOTING_DISABLED : "trial-voting-disabled",
    VERDICT_INNOCENT : "verdict-innocent",
    VERDICT_GUILTY : "verdict-guilty",
    DAY_TRIAL_CLOSED : "day-trial-closed",
    DAY_END : "day_end",
    BEGIN_NIGHT : "begin-night",
    END_NIGHT : "end-night",
    MESSAGE : "message",
    DEAD_MESSAGE : "dead-message"
};

var ERROR_RESPONSE = {
    ALIAS_TAKEN : "alias-taken"
};

function init() {
    domainName = "18.221.94.129";
    onTrial = null;
    aliasWindow = document.getElementById("alias-window");
    aliasInput = document.getElementById("alias-input");
    aliasSendButton = document.getElementById("alias-send-button");
    chatWindow = document.getElementById("chat-window");
    chatInput = document.getElementById("chat-input");
    chatSendButton = document.getElementById("chat-send-button");
    targetWindow = document.getElementById("target-window");

    authenticateAndConnect();
}

function authenticateAndConnect() {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", "http://" + domainName + "/php/get_auth.php");
    httpRequest.onreadystatechange = function() {
	if(httpRequest.readyState == XMLHttpRequest.DONE) {
	    if(httpRequest.status == 200) {
		connect(httpRequest.responseText);
	    } else {
		alert("There was a problem fetching the authenticator: (HTTP status: " + httpRequest.status + ")");
	    }
	}
    }
    httpRequest.send();
}

function connect(authenticator) {
    myUserName = authenticator.substring(0, authenticator.indexOf(':'));
    websocket = new WebSocket("ws://" + domainName + ":8080/GameServer/game");
    websocket.onopen = function(e) {
	websocket.send(NORMAL_MESSAGE.AUTHENTICATOR + authenticator);
    }
    websocket.onmessage = function(e) {
	console.log(e.data);
	if(e.data.startsWith(ERROR_RESPONSE.ALIAS_TAKEN)) {
	    showErrorMessage(aliasWindow, "Alias is taken.");
	} else if(e.data.startsWith(NORMAL_RESPONSE.ALIAS)) {
	    var alias = e.data.substring(NORMAL_RESPONSE.ALIAS.length);
	    showMessage(aliasWindow, alias + " has joined the game.");
	} else if(e.data.startsWith(NORMAL_RESPONSE.AUTHENTICATED)) {
	    sendGameToken();
	} else if(e.data.startsWith(NORMAL_RESPONSE.TOKEN_RECEIVED)) {
	    aliasSendButton.onclick = function() {
		if(aliasInput.value.length != 0) {
		    websocket.send(NORMAL_MESSAGE.ALIAS + aliasInput.value);
		    aliasInput.value = "";
		}
	    };
	} else if(e.data.startsWith(NORMAL_RESPONSE.FIRST_DAY)) {
	    var playersJSON = e.data.substring(NORMAL_RESPONSE.FIRST_DAY.length);
	    populatePlayers(playersJSON);
	} else if(e.data.startsWith(NORMAL_RESPONSE.CASUALTIES)) {
	    var casualtiesJSON = e.data.substring(NORMAL_RESPONS.CASUALTIES.length);
	    handleCasualties(casualtiesJSON);
	} else if(e.data.startsWith(NORMAL_RESPONSE.CURRENT_STATE)) {
	    var newStateJSON = e.data.substring(NORMAL_RESPONSE.CURRENT_STATE.length);
	    updateState(newStateJSON);
	} else if(e.data.startsWith(NORMAL_RESPONSE.CHAT_ENABLED)) {
	    enableChat();
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_OPEN)) {
	    showVoteButtons();
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_VOTE_ADDED)) {
	    var voter = e.data.substring(NORMAL_RESPONSE.TRIAL_VOTE_ADDED.length, e.data.indexOf(':'));
	    var target = e.data.substring(e.data.indexOf(':') + 1);
	    addVote(target);
	    showMessage(chatWindow, voter + " voted to put " + target + " on trial.");
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_VOTE_REMOVED)) {
	    var voter = e.data.substring(NORMAL_RESPONSE.TRIAL_VOTE_REMOVED.length, e.data.indexOf(':'));
	    var target = e.data.substring(e.data.indexOf(':') + 1);
	    removeVote(target);
	    showMessage(chatWindow, voter + " took back his vote.");  
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_INTRO)) {
	    var defenderJSON = e.data.substring(NORMAL_RESPONSE.TRIAL_INTRO.length);
	    onTrial = JSON.parse(defenderJSON);
	    disableChat();
	    hideVoteButtons();
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_DEFENSE)) {
	    var defender = e.data.substring(NORMAL_RESPONSE.TRIAL_DEFENSE.length);
	    showMessage(chatWindow, defender + ", what do you have to say to your defense?");
	    if(myUserName == defender) {
		enableChat();
	    }
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_VOTING_ENABLED)) {
	    enableChat();
	} else if(e.data.startsWith(NORMAL_RESPONSE.TRIAL_VOTING_DISABLED)) {
	    disableChat();
	} else if(e.data.startsWith(NORMAL_RESPONSE.VERDICT_INNOCENT)) {
	    onTrial = null;
	} else if(e.data.startsWith(NORMAL_RESPONSE.VERDICT_GUILTY)) {
	    removePlayer(onTrial.getNumber());
	} else if(e.data.startsWith(NORMAL_RESPONSE.DAY_TRIAL_CLOSED)) {
	    enableChat();
	} else if(e.data.startsWith(NORMAL_RESPONSE.DAY_END)) {
	    disableChat();
	} else if(e.data.startsWith(NORMAL_RESPONSE.BEGIN_NIGHT)) {
	    var validTargets = e.data.substring(NORMAL_RESPONSE.BEGIN_NIGHT).split();
	    enableChat();
	    showTargetButtons(validTargets);
	} else if(e.data.startsWith(NORMAL_RESPONSE.END_NIGHT)) {
	    disableChat();
	    hideTargetButtons();
	} else if(e.data.startsWith(NORMAL_RESPONSE.MESSAGE)) {
	    var message = e.data.substring(NORMAL_RESPONSE.MESSAGE.length);
	    showMessage(chatWindow, message);
	} else if(e.data.startsWith(NORMAL_RESPONSE.DEAD_MESSAGE)) {
	    var message = e.data.substring(NORMAL_RESPONSE.DEAD_MESSAGE.length);
	    showMessage(chatWindow, message);
	}
    }
    websocket.onclose = function(e) {
	alert(e.reason);
    }
    websocket.onerror = function(e) {
	alert(e.data);
    }
}

function populatePlayers(playersJSON) {
    var players = JSON.parse(playersJSON);
    for(var i = 0; i < players.length; i++) {
	var li = document.createElement("li");
	var p = document.createElement("p");
	var voteButton = document.createElement("div");
	var targetButton = document.createElement("div");
	var secondTargetButton = document.createElement("div");
	voteButton.style.display = "none";
	targetButton.style.display = "none";
	secondTargetButton.style.display = "none";
	voteButton.classList.add("vote-button");
	voteButton.dataset.votes = 0;
	targetButton.classList.add("target-button");
	secondTargetButon.classList.add("second-target-button");
	var p_vote = document.createElement("p");
	p_vote.innerHTML = "Vote (0)";
	voteButton.appendChild(p_vote);
	voteButton.addEventListener("click", function() {
	    if(this.classList.contains("selected")) {
		websocket.send(NORMAL_MESSAGE.TRIAL_VOTE_REMOVE);
	    } else {
		websocket.send(NORMAL_MESSAGE.TRIAL_VOTE_ADD + this.parentElement.dataset.number);
	    }
	    this.classList.toggle("selected");
	});
	targetButton.addEventListener("click", function() {
	    if(this.classList.contains("selected")) {
		websocket.send(NORMAL_MESSAGE.REMOVE_TARGET);
	    } else {
		websocket.send(NORMAL_MESSAGE.TARGET + this.parentElement.dataset.number);
	    }
	    this.classList.toggle("selected");
	});

	secondTargetButton.addEventListener("click", function() {
	    if(this.classList.contains("selected")) {
		websocket.send(NORMAL_MESSAGE.REMOVE_SECOND_TARGET);
	    } else {
		websocket.send(NORMAL_MESSAGE.SECOND_TARGET + this.parentElement.dataset.number);
	    }
	    this.classList.toggle("selected");
	});
	
	p.innerHTML = players[i].number + ". " + players[i].name;
	li.appendChild(p);
	li.appendChild(voteButton);
	li.appendChild(targetButton);
	li.appendChild(secondTargetButton);
	li.dataset.name = players[i].name;
	li.dataset.number = players[i].number;
	playerWindow.appendChild(li);
    }
}

function handleCasualties(casualtiesJSON) {
    var casualties = JSON.parse(casualtiesJSON);
    for(var i = 0; i < casualties.length; i++) {
	removePlayer(casualties[i]);
    }
}

function updateState(newStateJSON) {
    var state = JSON.parse(newStateJSON);
    var alive = state.alive;
    var dead = state.dead;
    var players = document.querySelectorAll("#player-window > li");
    outerloop1:
    for(var i = 0; i < alive.length; i++) {
	for(var j = 0; j < players.length; j++) {
	    if(alive[i].name == players[j].name) {
		players[j].style.visibility = "visible";
		continue outerloop1;
	    }
	}
    }
    outerloop2:
    for(var i = 0; i < dead.length; i++) {
	for(var j = 0; j < players.length; j++) {
	    if(dead[i].name == players[j].name) {
		players[j].style.visibility = "hidden";
		continue outerloop2;
	    }
	}
    }
}

function showVoteButtons() {
    var voteButtons = document.querySelectorAll(".vote-button");
    for(var i = 0; i < voteButtons.length; i++) {
	voteButtons[i].style.display = "inline-block";
    }
}

function hideVoteButtons() {
    var voteButtons = document.querySelectorAll(".vote-button");
    for(var i = 0; i < voteButtons.length; i++) {
	voteButtons[i].style.display = "none";
    }
}

function addVote(player) {
    var voteButton = document.querySelector("li[name='" + player + "' > div.vote-button");
    voteButton.dataset.votes++;
    voteButton.firstElementChild.innerHTML = "Vote ( " + voteButton.dataset.votes + ")";
}

function removeVote(player) {
    var voteButton = document.querySelector("li[name='" + player + "'] > div.vote-button");
    voteButton.dataset.votes--;
    voteButton.firstElementChild.innerHTML = "Vote ( " + voteButton.dataset.votes + ")";
}

function removePlayer(number) {
    document.querySelector("li[number=" + number + "]").style.visibility = "hidden";
}

function disableChat() {
    chatSendButton.onclick = function() {
	return false;
    }
}

function enableChat() {
    chatSendButton.onclick = function() {
	if(chatInput.value.length != 0) {
	    websocket.send(NORMAL_MESSAGE.MESSAGE + chatInput.value);
	    chatInput.value = "";
	}
    };
}

function sendGameToken() {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", "http://" + domainName + "/php/get_game_token.php");
    httpRequest.onreadystatechange = function() {
	if(httpRequest.readyState == XMLHttpRequest.DONE) {
	    if(httpRequest.status == 200) {
		websocket.send(NORMAL_MESSAGE.GAME_TOKEN + httpRequest.responseText);
	    } else {
		alert("There was a problem fetching the game token: (HTTP status: " + httpRequest.status + ")");
	    }
	}
    }
    httpRequest.send();
}

function keyPress(e) {
    if(e.keyCode == 13) {
	if(aliasWrapper.style.display != "none") {
	    aliasSendButton.click();
	} else if(chatWrapper.style.display != "none") {
	    chatSendButton.click();
	}
	return false;
    }
    return true;
}

function showMessage(window, message) {
    var p = document.createElement('p');
    p.innerHTML = message;
    if(window.scrollTop + window.clientHeight == window.scrollHeight) {
	window.appendChild(p);
	window.scrollTop = window.scrollHeight;
    } else {
	window.appendChild(p);
    }
}

function showErrorMessage(window, message) {
    var p = document.createElement('p');
    p.innerHTML = message;
    p.style.color = "red";
    if(window.scrollTop + window.clientHeight == window.scrollHeight) {
	window.appendChild(p);
	window.scrollTop = messageWindow.scrollHeight;
    } else {
	window.appendChild(p);
    }
}
